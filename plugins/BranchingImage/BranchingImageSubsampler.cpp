#define SOFA_BRANCHINGIMAGE_BranchingImageSubsampler_CPP

#include "BranchingImageSubsampler.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(BranchingImageSubsampler)

int BranchingImageSubsamplerClass = core::RegisterObject("Returns a subsampled branching image and volumes of coarse cells. Each coarse cell represents a connected region of fine cells (with similar intensities if spliLabels enabled).")
        .add<BranchingImageSubsampler<BranchingImageUC> >(true)
        .add<BranchingImageSubsampler<BranchingImageUI> >()
        .add<BranchingImageSubsampler<BranchingImageB> >()
        .add<BranchingImageSubsampler<BranchingImageD> >()
        .add<BranchingImageSubsampler<BranchingImageUS> >()
        ;

template class SOFA_BRANCHINGIMAGE_API BranchingImageSubsampler<BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageSubsampler<BranchingImageUI>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageSubsampler<BranchingImageB>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageSubsampler<BranchingImageD>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageSubsampler<BranchingImageUS>;

} //
} // namespace component
} // namespace sofa

