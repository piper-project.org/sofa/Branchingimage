#define SOFA_BRANCHINGIMAGE_BranchingCellVolumes_CPP

#include "BranchingCellVolumes.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(BranchingCellVolumes)

int BranchingCellVolumesClass = core::RegisterObject("Returns volumes of branching voxels, given a fine image of superimposed offsets")
        .add<BranchingCellVolumes<ImageUI,BranchingImageUC> >(true)
        .add<BranchingCellVolumes<ImageUI,BranchingImageB> >()
        .add<BranchingCellVolumes<ImageUI,BranchingImageD> >()
        .add<BranchingCellVolumes<ImageUI,BranchingImageUS> >()
        ;

template class SOFA_BRANCHINGIMAGE_API BranchingCellVolumes<ImageUI,BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellVolumes<ImageUI,BranchingImageB>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellVolumes<ImageUI,BranchingImageD>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellVolumes<ImageUI,BranchingImageUS>;

} //
} // namespace component
} // namespace sofa

