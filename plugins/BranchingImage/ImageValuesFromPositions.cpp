#define SOFA_BRANCHINGIMAGE_ImageValuesFromPositions_CPP

#include "ImageValuesFromPositions.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS (BranchingImageValuesFromPositions);

int BranchingImageValuesFromPositionsClass = core::RegisterObject("Get image intensities at sample locations")
        .add<ImageValuesFromPositions<BranchingImageD> >()
        .add<ImageValuesFromPositions<BranchingImageUC> >()
#ifdef BUILD_ALL_IMAGE_TYPES
        .add<ImageValuesFromPositions<BranchingImageC> >()
        .add<ImageValuesFromPositions<BranchingImageI> >()
        .add<ImageValuesFromPositions<BranchingImageUI> >()
        .add<ImageValuesFromPositions<BranchingImageS> >()
        .add<ImageValuesFromPositions<BranchingImageUS> >()
        .add<ImageValuesFromPositions<BranchingImageL> >()
        .add<ImageValuesFromPositions<BranchingImageUL> >()
        .add<ImageValuesFromPositions<BranchingImageF> >()
        .add<ImageValuesFromPositions<BranchingImageB> >()
#endif
        ;

template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageD>;
template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageUC>;
#ifdef BUILD_ALL_IMAGE_TYPES
template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageC>;
template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageI>;
template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageUI>;
template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageS>;
template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageUS>;
template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageL>;
template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageUL>;
template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageF>;
template class SOFA_BRANCHINGIMAGE_API ImageValuesFromPositions<BranchingImageB>;
#endif

} //
} // namespace component

} // namespace sofa

