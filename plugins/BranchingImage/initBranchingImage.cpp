#include <BranchingImage/config.h>
#include <sofa/helper/system/config.h>

namespace sofa
{

namespace component
{

//Here are just several convenient functions to help user to know what contains the plugin

extern "C" {
    SOFA_BRANCHINGIMAGE_API void initExternalModule();
    SOFA_BRANCHINGIMAGE_API const char* getModuleName();
    SOFA_BRANCHINGIMAGE_API const char* getModuleVersion();
    SOFA_BRANCHINGIMAGE_API const char* getModuleLicense();
    SOFA_BRANCHINGIMAGE_API const char* getModuleDescription();
    SOFA_BRANCHINGIMAGE_API const char* getModuleComponentList();
}

void initExternalModule()
{
    static bool first = true;
    if (first)
    {
        first = false;
    }
}

const char* getModuleName()
{
    return "BranchingImage Plugin";
}

const char* getModuleVersion()
{
    return "0.1";
}

const char* getModuleLicense()
{
    return "QGPL";
}


const char* getModuleDescription()
{
    return "Branching Image support in SOFA";
}

const char* getModuleComponentList()
{
    /// string containing the names of the classes provided by the plugin
    return "";
    //return "MyMappingPendulumInPlane, MyBehaviorModel, MyProjectiveConstraintSet";
}



}

}

