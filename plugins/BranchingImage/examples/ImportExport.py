import Sofa
import SofaPython.Tools


filename = SofaPython.Tools.localPath(__file__,"testbi.bia")

def createScene(rootNode):

    rootNode.createObject('PythonScriptController', classname="ImportController")

    rootNode.createObject('RequiredPlugin', pluginName='image')
    rootNode.createObject('RequiredPlugin', pluginName='BranchingImage')

    # import image
    rootNode.createObject('ImageContainer', filename='textures/cubemap_bk.bmp', drawBB='false', transform='-5 -5 0 0 0 0 0.1 0.1 30 0 1 1', name='image')

    # convert to branching image
    rootNode.createObject('ImageToBranchingImageConverter', superimpositionType='0', name='converter', template='ImageUC,BranchingImageUC', inputImage='@image.image', coarseningLevels='0', inputTransform='@image.transform', printLog='true')
    rootNode.createObject('ImageContainer', branchingImage='@converter.branchingImage', drawBB='false', name='branchingImage', template='BranchingImageUC', transform='@converter.transform')

    # export branching image
    rootNode.createObject('ImageExporter', exportAtBegin='true', filename=filename, name='writerbi', template='BranchingImageUC', src='@branchingImage')




class ImportController(Sofa.PythonScriptController):

    def bwdInitGraph(self,node):

        # import branching image
        node.createObject('ImageContainer', name='branchingImage2', template='BranchingImageUC', filename=filename, drawBB=True, warning=False)

        # verif (convert to regular image + viewer)
        verif = node.createChild("verif")
        verif.createObject('BranchingImageToImageConverter', inputBranchingImage='@branchingImage2.branchingImage', printLog='true', name='imageConverter', conversionType='0', template='BranchingImageUC,ImageUC')
        verif.createObject('ImageContainer', image='@imageConverter.image', drawBB='false', name='imagebi2', template='ImageUC', transform='@branchingImage2.transform')
        verif.createObject('ImageViewer', src='@imagebi2', name='viewerbi2', template='ImageUC')
        verif.init()
