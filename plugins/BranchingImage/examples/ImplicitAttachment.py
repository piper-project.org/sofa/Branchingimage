import Sofa

def createScene(rootNode):
    
    rootNode.createObject('RequiredPlugin',pluginName="Flexible")
    rootNode.createObject('RequiredPlugin',pluginName="image")
    rootNode.createObject('RequiredPlugin',pluginName="BranchingImage")
    
    rootNode.createObject('VisualStyle',displayFlags="showVisual showBehaviorModels showForceFields hideWireframe")
    rootNode.createObject('DefaultAnimationLoop')
    rootNode.createObject('DefaultVisualManagerLoop')
    
    rootNode.createObject('MeshObjLoader',name="cyl",filename="mesh/cylinder_open.obj",triangulate="1")
    rootNode.createObject('MeshClosingEngine',name="closedcyl",inputPosition="@cyl.position",inputTriangles="@cyl.triangles")
    rootNode.createObject('MeshToImageEngine',name="cyl_im",template="ImageUC",position="@closedcyl.position",triangles="@closedcyl.triangles",voxelSize=".18",padSize="1",rotateImage="false",value="1",insideValue="1",roiValue="2",roiIndices="@closedcyl.indices")

    rootNode.createObject('MeshObjLoader',filename="mesh/cubeQuad.obj",translation="0 -1 0",triangulate="1")
    rootNode.createObject('MeshObjLoader',filename="mesh/cubeQuad.obj",translation="0 11 0",triangulate="1")
    rootNode.createObject('MeshObjLoader',filename="mesh/cubeQuad.obj",translation="2 5 0",scale3d="1 7 1",triangulate="1")
    rootNode.createObject('MergeMeshes',name="fr",nbMeshes="3",position1="@[-1].position",triangles1="@[-1].triangles",position2="@[-2].position",triangles2="@[-2].triangles",position3="@[-3].position",triangles3="@[-3].triangles")
    rootNode.createObject('MeshToImageEngine',name="fr_im",template="ImageUC",position="@fr.position",triangles="@fr.triangles",voxelSize=".18",padSize="1",rotateImage="false",value="3",insideValue="3")

    rootNode.createObject('MergeImagesIntoBranching',name="label",nbImages="2",image1="@cyl_im.image",transform1="@cyl_im.transform",image2="@fr_im.image",transform2="@fr_im.transform",connectLabels="2 3")
    
    # rootNode.createObject('BranchingImageToImageConverter',inputBranchingImage="@label.branchingImage",conversionType="3")
    # rootNode.createObject('ImageViewer',image="@[-1].image",transform="@label.transform")

    # ConvertlabeltoYoungModulus
    rootNode.createObject('TransferFunction',name="youngTF",template="BranchingImageUC,BranchingImageD",inputImage="@label.branchingImage",param="0 0 1 100 2 100 3 10000000")
    rootNode.createObject('ImageContainer',template="BranchingImageD",name="youngM",branchingImage="@youngTF.outputImage",transform="@label.transform",drawBB="false")


    # Simu
    rootNode.createObject('EulerImplicit')
    rootNode.createObject('CGLinearSolver',iterations="25",tolerance="1E-5",threshold="1E-5")
    rootNode.gravity="0 0 -10"
    rootNode.dt="0.1"

    rootNode.createObject('ImageSampler',template="BranchingImageD",name="sampler",src="@youngM",method="1",param="5 1",fixedPosition="2 5 0")
    rootNode.createObject('MergeMeshes',name="mergedSamples",nbMeshes="2",position1="@sampler.fixedPosition",position2="@sampler.position")
    rootNode.createObject('MechanicalObject',template="Affine",name="dof",showObject="true",showObjectScale="1.2",src="@mergedSamples")
    rootNode.createObject('FixedConstraint',indices="0")

    rootNode.createObject('VoronoiShapeFunction',name="SF",template="ShapeFunctiond,BranchingImageD",position="@dof.rest_position",src="@youngM",method="0",nbRef="4",bias="true")
    # VisualizationShapeFunctiondata
    # rootNode.createObject('BranchingImageToImageConverter',template="BranchingImageD,ImageD",inputBranchingImage="@SF.weights",conversionType="0")
    # rootNode.createObject('ImageViewer',template="ImageD",image="@[-1].image",transform="@image.transform")

    bnode = rootNode.createChild("behavior")
    bnode.createObject('ImageGaussPointSampler',name="sampler",template="BranchingImageD,BranchingImageUC",indices="@../SF.indices",weights="@../SF.weights",transform="@../SF.transform",method="2",order="4",targetNumber="1")
    bnode.createObject('MechanicalObject',template="F332")
    bnode.createObject('LinearMapping',template="Affine,F332")
    enode = bnode.createChild("E")
    enode.createObject('ImageValuesFromPositions',name="youngM",template="BranchingImageD",position="@../sampler.position",src="@../../../youngM",interpolation="0")
    enode.createObject('MechanicalObject',template="E332",name="E")
    enode.createObject('CorotationalStrainMapping',template="F332,E332")
    enode.createObject('HookeForceField',template="E332",name="ff",youngModulus="@youngM.values",poissonRatio="0",viscosity="0")

    cnode = rootNode.createChild("collision1")
    cnode.createObject('VisualStyle',displayFlags="hideBehaviorModels")
    cnode.createObject('Mesh',name="mesh",src="@../cyl")
    cnode.createObject('MechanicalObject',template="Vec3d",name="pts")
    cnode.createObject('UniformMass',totalMass="20")
    cnode.createObject('BranchingCellOffsetsFromPositions',name="cell",template="BranchingImageUC",position="@mesh.position",src="@../label",labels="1 2")
    cnode.createObject('LinearMapping',template="Affine,Vec3d",cell="@cell.cell")
    vnode = cnode.createChild("visual")
    vnode.createObject('VisualModel',color="10.50.51")
    vnode.createObject('IdentityMapping')

    cnode = rootNode.createChild("collision2")
    cnode.createObject('VisualStyle',displayFlags="hideBehaviorModels")
    cnode.createObject('Mesh',name="mesh",src="@../fr")
    cnode.createObject('MechanicalObject',template="Vec3d",name="pts")
    cnode.createObject('UniformMass',totalMass="20")
    cnode.createObject('BranchingCellOffsetsFromPositions',name="cell",template="BranchingImageUC",position="@mesh.position",src="@../label",labels="3")
    cnode.createObject('LinearMapping',template="Affine,Vec3d",cell="@cell.cell")
    vnode = cnode.createChild("visual")
    vnode.createObject('VisualModel',color="0.50.511")
    vnode.createObject('IdentityMapping')

    return rootNode
    
    
