#define SOFA_BRANCHINGIMAGE_IMAGEEXPORTER_CPP


#include "ImageExporter.h"
#include <sofa/core/ObjectFactory.h>


namespace sofa
{

namespace component
{

namespace misc
{

using namespace defaulttype;

SOFA_DECL_CLASS (BranchingImageExporter);

int BranchingImageExporterClass = core::RegisterObject("Save an image")
        .add<ImageExporter<BranchingImageUC> >()
        .add<ImageExporter<BranchingImageD> >()
#ifdef BUILD_ALL_IMAGE_TYPES
        .add<ImageExporter<BranchingImageC> >()
        .add<ImageExporter<BranchingImageI> >()
        .add<ImageExporter<BranchingImageUI> >()
        .add<ImageExporter<BranchingImageS> >()
        .add<ImageExporter<BranchingImageUS> >()
        .add<ImageExporter<BranchingImageL> >()
        .add<ImageExporter<BranchingImageUL> >()
        .add<ImageExporter<BranchingImageF> >()
        .add<ImageExporter<BranchingImageB> >()
#endif
        ;

template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageD>;
#ifdef BUILD_ALL_IMAGE_TYPES
template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageC>;
template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageI>;
template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageUI>;
template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageS>;
template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageUS>;
template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageL>;
template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageUL>;
template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageF>;
template class SOFA_BRANCHINGIMAGE_API ImageExporter<BranchingImageB>;
#endif


} // namespace misc

} // namespace component

} // namespace sofa
