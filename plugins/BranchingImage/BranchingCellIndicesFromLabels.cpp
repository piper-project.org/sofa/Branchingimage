#define SOFA_BRANCHINGIMAGE_BranchingCellIndicesFromLabels_CPP

#include "BranchingCellIndicesFromLabels.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(BranchingCellIndicesFromLabels)

int BranchingCellIndicesFromLabelsClass = core::RegisterObject("Returns global index of branching image voxels given a list of labels")
        .add<BranchingCellIndicesFromLabels<BranchingImageUC> >(true)
        .add<BranchingCellIndicesFromLabels<BranchingImageUS> >()
        .add<BranchingCellIndicesFromLabels<BranchingImageUI> >()
        ;

template class SOFA_BRANCHINGIMAGE_API BranchingCellIndicesFromLabels<BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellIndicesFromLabels<BranchingImageUS>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellIndicesFromLabels<BranchingImageUI>;

} //
} // namespace component
} // namespace sofa

