import Sofa
import SofaImage.API

class Image(SofaImage.API.Image):
    def __init__(self, parentNode, name='', imageType="ImageUC"):
        SofaImage.API.Image.__init__(self, parentNode, name, imageType)
        self.prefix = "Branching"
        self.converter = None


    def fromImage(self,im,coarseningLevels="0", superimpositionType="1", createFineImage="False"):
        """ initialize from a SofaImage.API.Image object
        """
        if im.image is None:
            Sofa.msg_error("BranchingImage.API.Image","addImageToBranching: no image")
            return
        self.image = self.node.createObject("ImageToBranchingImageConverter", template=im.template()+","+self.template(), name ="branchingImage", inputImage=im.getImagePath()+".image", inputTransform=im.getImagePath()+".transform", coarseningLevels=coarseningLevels, superimpositionType=superimpositionType, createFineImage=createFineImage)

    def toImage(self,node, name, conversionType=0):
        """ returns a SofaImage.API.Image object
        """
        self.addBranchingToImage(conversionType)
        im = SofaImage.API.Image(node, name,self.imageType)
        im.image = self.node.createObject("ImageContainer", template=im.template(), name="fromBranching", image=self.converter.getLinkPath()+".image", transform=self.getImagePath()+".transform", drawBB="false")
        # im.node.addChild(self.node)
        return im

    def fromImages(self, images,  connectLabels=""):
        """ initialize from several SofaImage.API.Image objects
        """
        args=dict()
        i=1
        for im in images:
            args["image"+str(i)]=im.getImagePath()+".image"
            args["transform"+str(i)]=im.getImagePath()+".transform"
            i+=1
        self.image = self.node.createObject("MergeImagesIntoBranching", template=images[0].template()+","+self.template(), name="mergedImage", nbImages=len(images), connectLabels=connectLabels, connectivity="27", **args )
        for parentImage in images:
            parentImage.node.addChild(self.node)


    def fromSubsampledImage(self,im,coarseningFactor):
        """ initialize by subsampling a branching image
            note: using a different name than container allows inplace use of this function
        """
        self.image = self.node.createObject("BranchingImageSubsampler", template=im.template(), name ="subsampledImage", inputImage=im.getImagePath()+".image", inputTransform=im.getImagePath()+".transform", coarseningFactor=coarseningFactor, splitLabels="true")


    def addBranchingToImage(self, conversionType=0):
        if not self.converter is None: # already added ?
            return 
        if self.image is None:
            Sofa.msg_error("BranchingImage.API.Image","addBranchingToImage: no image")
            return
        self.converter = self.node.createObject("BranchingImageToImageConverter", template=self.template()+","+self.imageType, name="converter", inputBranchingImage=self.getImagePath()+".branchingImage", conversionType=conversionType)

    def addViewer(self):
        self.addBranchingToImage()
        if not self.converter is None:
            self.viewerContainer = self.node.createObject("ImageContainer", template=self.imageType, name="image", image=self.converter.getLinkPath()+".image", transform=self.getImagePath()+".transform", drawBB="false")
            self.viewer = self.node.createObject("ImageViewer", name="biViewer", template=self.imageType, src=self.viewerContainer.getLinkPath())
