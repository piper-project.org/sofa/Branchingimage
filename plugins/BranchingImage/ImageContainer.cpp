#define SOFA_BRANCHINGIMAGE_IMAGECONTAINER_CPP

#include "ImageContainer.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace container
{

using namespace defaulttype;


SOFA_DECL_CLASS (BranchingImageContainer);
// Register in the Factory

int BranchingImageContainerClass = core::RegisterObject ( "Image Container" )
        .add<ImageContainer<BranchingImageUC> >()
        .add<ImageContainer<BranchingImageD> >()
#ifdef BUILD_ALL_IMAGE_TYPES
        .add<ImageContainer<BranchingImageC> >()
        .add<ImageContainer<BranchingImageI> >()
        .add<ImageContainer<BranchingImageUI> >()
        .add<ImageContainer<BranchingImageS> >()
        .add<ImageContainer<BranchingImageUS> >()
        .add<ImageContainer<BranchingImageL> >()
        .add<ImageContainer<BranchingImageUL> >()
        .add<ImageContainer<BranchingImageF> >()
        .add<ImageContainer<BranchingImageB> >()
#endif
        ;

template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageD>;
#ifdef BUILD_ALL_IMAGE_TYPES
template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageC>;
template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageI>;
template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageUI>;
template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageS>;
template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageUS>;
template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageL>;
template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageUL>;
template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageF>;
template class SOFA_BRANCHINGIMAGE_API ImageContainer<BranchingImageB>;
#endif

} // namespace container

} // namespace component

} // namespace sofa
