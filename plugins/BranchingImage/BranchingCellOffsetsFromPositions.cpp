#define SOFA_BRANCHINGIMAGE_BranchingCellOffsetsFromPositions_CPP

#include "BranchingCellOffsetsFromPositions.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(BranchingCellOffsetsFromPositions)

int BranchingCellOffsetsFromPositionsClass = core::RegisterObject("Returns offsets of superimposed voxels at positions corresponding to given labels (image intensity values)")
        .add<BranchingCellOffsetsFromPositions<BranchingImageUC> >(true)
        .add<BranchingCellOffsetsFromPositions<BranchingImageB> >()
        .add<BranchingCellOffsetsFromPositions<BranchingImageD> >()
        .add<BranchingCellOffsetsFromPositions<BranchingImageUS> >()
        ;

template class SOFA_BRANCHINGIMAGE_API BranchingCellOffsetsFromPositions<BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellOffsetsFromPositions<BranchingImageB>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellOffsetsFromPositions<BranchingImageD>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellOffsetsFromPositions<BranchingImageUS>;

} //
} // namespace component
} // namespace sofa

