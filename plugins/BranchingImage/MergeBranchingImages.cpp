#define SOFA_BRANCHINGIMAGE_MERGEBRANCHINGIMAGES_CPP

#include "MergeBranchingImages.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(MergeBranchingImages)

int MergeBranchingImagesClass = core::RegisterObject("Merge branching images")
        .add<MergeBranchingImages<BranchingImageUC> >(true)
        .add<MergeBranchingImages<BranchingImageD> >()
#ifdef BUILD_ALL_IMAGE_TYPES
        .add<MergeBranchingImages<BranchingImageC> >()
        .add<MergeBranchingImages<BranchingImageI> >()
        .add<MergeBranchingImages<BranchingImageUI> >()
        .add<MergeBranchingImages<BranchingImageS> >()
        .add<MergeBranchingImages<BranchingImageUS> >()
        .add<MergeBranchingImages<BranchingImageL> >()
        .add<MergeBranchingImages<BranchingImageUL> >()
        .add<MergeBranchingImages<BranchingImageF> >()
        .add<MergeBranchingImages<BranchingImageB> >()
#endif
        ;

template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageD>;
#ifdef BUILD_ALL_IMAGE_TYPES
template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageC>;
template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageI>;
template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageUI>;
template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageS>;
template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageUS>;
template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageL>;
template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageUL>;
template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageF>;
template class SOFA_BRANCHINGIMAGE_API MergeBranchingImages<BranchingImageB>;
#endif


} //
} // namespace component

} // namespace sofa

