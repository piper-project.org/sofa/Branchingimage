#include <sofa/defaulttype/TemplatesAliases.h>

#include "BranchingImage.h"

namespace sofa
{
namespace defaulttype
{

#ifndef SOFA_FLOAT
RegisterTemplateAlias BranchingImageRAlias("BranchingImageR", "BranchingImageD");
#else
RegisterTemplateAlias BranchingImageRAlias("BranchingImageR", "BranchingImageF");
#endif

}
}
