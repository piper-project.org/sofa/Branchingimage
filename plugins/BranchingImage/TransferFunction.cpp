#define SOFA_BRANCHINGIMAGE_TRANSFERFUNCTION_CPP

#include "TransferFunction.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{
namespace component
{
namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(BranchingTransferFunction)

int BranchingTransferFunctionClass = core::RegisterObject("Transforms pixel intensities")
.add<TransferFunction<BranchingImageUC,BranchingImageUC    > >()
.add<TransferFunction<BranchingImageD ,BranchingImageD     > >()
.add<TransferFunction<BranchingImageUC,BranchingImageD    > >()
.add<TransferFunction<BranchingImageD,BranchingImageUC    > >()
.add<TransferFunction<BranchingImageUC,BranchingImageUI    > >()
.add<TransferFunction<BranchingImageUC,BranchingImageUS    > >()
.add<TransferFunction<BranchingImageUS,BranchingImageUC    > >()
.add<TransferFunction<BranchingImageUC,BranchingImageB    > >()

#ifdef BUILD_ALL_IMAGE_TYPES
.add<TransferFunction<BranchingImageC ,BranchingImageC     > >()
.add<TransferFunction<BranchingImageI ,BranchingImageI     > >()
.add<TransferFunction<BranchingImageUI,BranchingImageUI    > >()
.add<TransferFunction<BranchingImageS ,BranchingImageS     > >()
.add<TransferFunction<BranchingImageUS,BranchingImageUS    > >()
.add<TransferFunction<BranchingImageL ,BranchingImageL     > >()
.add<TransferFunction<BranchingImageUL,BranchingImageUL    > >()
.add<TransferFunction<BranchingImageF ,BranchingImageF     > >()
.add<TransferFunction<BranchingImageB ,BranchingImageB     > >()

.add<TransferFunction<BranchingImageC ,BranchingImageD     > >()
.add<TransferFunction<BranchingImageI ,BranchingImageD     > >()
.add<TransferFunction<BranchingImageUI,BranchingImageD    > >()
.add<TransferFunction<BranchingImageS ,BranchingImageD     > >()
.add<TransferFunction<BranchingImageUS,BranchingImageD    > >()
.add<TransferFunction<BranchingImageL ,BranchingImageD     > >()
.add<TransferFunction<BranchingImageUL,BranchingImageD    > >()
.add<TransferFunction<BranchingImageF ,BranchingImageD     > >()
.add<TransferFunction<BranchingImageB ,BranchingImageD     > >()

.add<TransferFunction<BranchingImageUC,BranchingImageF    > >()
#endif
        ;


template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUC  ,BranchingImageUC    >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageD   ,BranchingImageD     >;

template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUC  ,BranchingImageD    >;

template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageD  ,BranchingImageUC    >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUC   ,BranchingImageUI     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUC   ,BranchingImageUS     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUS   ,BranchingImageUC     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUC   ,BranchingImageB     >;

#ifdef BUILD_ALL_IMAGE_TYPES
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageC   ,BranchingImageC     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageI   ,BranchingImageI     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUI  ,BranchingImageUI    >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageS   ,BranchingImageS     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUS  ,BranchingImageUS    >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageL   ,BranchingImageL     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUL  ,BranchingImageUL    >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageF   ,BranchingImageF     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageB   ,BranchingImageB     >;

template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageC   ,BranchingImageD     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageI   ,BranchingImageD     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUI  ,BranchingImageD    >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageS   ,BranchingImageD     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUS  ,BranchingImageD    >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageL   ,BranchingImageD     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUL  ,BranchingImageD    >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageF   ,BranchingImageD     >;
template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageB   ,BranchingImageD     >;

template class SOFA_BRANCHINGIMAGE_API TransferFunction<BranchingImageUC  ,BranchingImageF    >;
#endif



} //
} // namespace component
} // namespace sofa

