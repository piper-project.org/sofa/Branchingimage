#ifndef SOFA_BRANCHINGIMAGE_MERGEBRANCHINGIMAGES_H
#define SOFA_BRANCHINGIMAGE_MERGEBRANCHINGIMAGES_H

#include <BranchingImage/config.h>
#include "BranchingImage.h"
#include <sofa/core/DataEngine.h>
#include <sofa/core/objectmodel/BaseObject.h>
#include <sofa/defaulttype/Vec.h>
#include <sofa/helper/vectorData.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace sofa
{

namespace component
{

namespace engine
{

/**
 * This class merges branching images into one
 */


template <class _ImageTypes>
class MergeBranchingImages : public core::DataEngine
{
public:
    typedef core::DataEngine Inherited;
    SOFA_CLASS(SOFA_TEMPLATE(MergeBranchingImages,_ImageTypes),Inherited);

    typedef _ImageTypes ImageTypes;
    typedef typename ImageTypes::T T;
    typedef typename ImageTypes::imCoord imCoord;
    typedef helper::WriteOnlyAccessor<Data< ImageTypes > > waImage;
    typedef helper::ReadAccessor<Data< ImageTypes > > raImage;

    typedef SReal Real;
    typedef defaulttype::ImageLPTransform<Real> TransformType;
    typedef typename TransformType::Coord Coord;
    typedef helper::WriteOnlyAccessor<Data< TransformType > > waTransform;
    typedef helper::ReadAccessor<Data< TransformType > > raTransform;

    Data<unsigned int> nbImages;

    helper::vectorData<ImageTypes> inputImages;
    helper::vectorData<TransformType> inputTransforms;

    Data<ImageTypes> image;
    Data<TransformType> transform;

    Data<helper::vector<T> > connectLabels;
    Data<unsigned> connectivity;

    virtual std::string getTemplateName() const    { return templateName(this);    }
    static std::string templateName(const MergeBranchingImages<ImageTypes>* = NULL) { return ImageTypes::Name(); }

    MergeBranchingImages()    :   Inherited()
      , nbImages ( initData ( &nbImages,(unsigned int)0,"nbImages","number of images to merge" ) )
      , inputImages(this, "image", "input image", helper::DataEngineInput)
      , inputTransforms(this, "transform", "input transform", helper::DataEngineInput)
      , image(initData(&image,ImageTypes(),"image","Image"))
      , transform(initData(&transform,TransformType(),"transform","Transform"))
      , connectLabels(initData(&connectLabels,"connectLabels","Pairs of label to be connected accross different input images"))
      , connectivity(initData(&connectivity,(unsigned)27,"connectivity","must be 1, 7 or 27 (27 by default or any incorrect value)"))
    {
        inputImages.resize(nbImages.getValue());
        inputTransforms.resize(nbImages.getValue());
        image.setReadOnly(true);
        transform.setReadOnly(true);
        this->addAlias(&image, "outputImage");
        this->addAlias(&image, "branchingImage");
        this->addAlias(&transform, "outputTransform");
        this->addAlias(&transform, "branchingTransform");
    }

    virtual ~MergeBranchingImages()
    {
    }

    virtual void init()
    {
        addInput(&nbImages);
        inputImages.resize(nbImages.getValue());
        inputTransforms.resize(nbImages.getValue());

        addOutput(&image);
        addOutput(&transform);

        setDirtyValue();
    }

    virtual void reinit()
    {
        inputImages.resize(nbImages.getValue());
        inputTransforms.resize(nbImages.getValue());
        update();
    }


    /// Parse the given description to assign values to this object's fields and potentially other parameters
    void parse ( sofa::core::objectmodel::BaseObjectDescription* arg )
    {
        inputImages.parseSizeData(arg, nbImages);
        inputTransforms.parseSizeData(arg, nbImages);
        Inherit1::parse(arg);
    }

    /// Assign the field values stored in the given map of name -> value pairs
    void parseFields ( const std::map<std::string,std::string*>& str )
    {
        inputImages.parseFieldsSizeData(str, nbImages);
        inputTransforms.parseFieldsSizeData(str, nbImages);
        Inherit1::parseFields(str);
    }


protected:

    virtual void update()
    {
        //        createInputImagesData();

        updateAllInputsIfDirty();
        cleanDirty();

        unsigned int nb = nbImages.getValue();
        if(!nb) return;

        // init BB
        defaulttype::Vec<2,Coord> BB = this->getBB(0);
        Coord minScale = this->getScale(0);
        for(unsigned int j=1; j<nb; j++)
        {
            defaulttype::Vec<2,Coord> bb = this->getBB(j);
            for(unsigned int k=0; k<bb[0].size(); k++)
            {
                if(BB[0][k]>bb[0][k]) BB[0][k]=bb[0][k];
                if(BB[1][k]<bb[1][k]) BB[1][k]=bb[1][k];
            }
            for(unsigned int k=0; k<3; k++)
                if( minScale[k] > this->getScale(j)[k] )
                {
                    minScale[k] = this->getScale(j)[k];
                    serr<<"MergeBranchingImages does not handle different voxel sizes"<<sendl;
                }
        }

        // get input Data
        helper::vector<const ImageTypes*> in;
        helper::vector<const TransformType*> inT;
        for(unsigned int j=0; j<nb; j++)
        {
            raImage inData(this->inputImages[j]);  in.push_back(&inData.ref());
            if(in.back()->isEmpty())  { this->serr<<"Image "<<j<<" not found"<<this->sendl; return; }
            raTransform inTData(this->inputTransforms[j]);   inT.push_back(&inTData.ref());
        }

        // init transform = translated version of inputTransforms[0] with minimum voxel size
        waTransform outT(this->transform);
        outT->operator=(*(inT[0]));
        outT->getTranslation()=BB[0];
        outT->getScale()=minScale;

        // init image
        imCoord dim=in[0]->getDimensions();
        Coord MaxP=outT->toImage(BB[1]); // corner pixel = dim-1
        dim[ImageTypes::DIMENSION_X]=ceil(MaxP[0])+1;
        dim[ImageTypes::DIMENSION_Y]=ceil(MaxP[1])+1;
        dim[ImageTypes::DIMENSION_Z]=ceil(MaxP[2])+1;

        waImage outData(this->image);  ImageTypes& img = outData.wref();
        img.setDimensions(dim);

        // fill image
        //        typedef typename ImageTypes::ConnectionVoxel ConnectionVoxel;
        typedef typename ImageTypes::VoxelIndex VoxelIndex;
        typedef typename ImageTypes::SuperimposedVoxels SuperimposedVoxels;

        helper::vector<unsigned int> sizes(img.getImageSize()); // buffer to record neighbor offsets due to previously pasted images

        for(unsigned int j=0; j<nb; j++)
        {
            const ImageTypes &inj=*(in[j]);

            bimg_forT(inj,t)
            {
                for(unsigned int i=0;i<sizes.size();i++) sizes[i] = img.imgList[t][i].size();

#ifdef _OPENMP
#pragma omp parallel for
#endif
                bimg_foroff1D(inj,i1dj)
                        if(inj.imgList[t][i1dj].size())
                {
                    const SuperimposedVoxels &Vj=inj.imgList[t][i1dj];
                    unsigned int Pj[3];     inj.index1Dto3D(i1dj,Pj[0],Pj[1],Pj[2]);
                    Coord P=outT->toImageInt( inT[j]->fromImage( Coord(Pj[0],Pj[1],Pj[2]) ) );

                    if(img.isInside((int)P[0],(int)P[1],(int)P[2]))
                    {
                        SuperimposedVoxels &V=img.imgList[t][ img.index3Dto1D(P[0],P[1],P[2]) ];
                        for( unsigned v=0 ; v<Vj.size() ; ++v )
                        {
                            V.push_back( Vj[v] , dim[ImageTypes::DIMENSION_S]);

                            for( unsigned n=0 ; n<Vj[v].neighbours.size() ; ++n )
                            {
                                VoxelIndex N=Vj[v].neighbours[n];
                                inj.index1Dto3D(N.index1d,Pj[0],Pj[1],Pj[2]);
                                P=outT->toImageInt( inT[j]->fromImage( Coord(Pj[0],Pj[1],Pj[2]) ) );
                                if(img.isInside((int)P[0],(int)P[1],(int)P[2]))
                                {
                                    N.index1d = img.index3Dto1D(P[0],P[1],P[2]);
                                    N.offset += sizes[N.index1d];
                                    V.last().neighbours[n] = N ;
                                }
                            }
                        }
                    }
                }
            }
        }


        // connect labels based on intensities of first channel
        helper::ReadAccessor<Data< helper::vector<T> > > connectL(this->connectLabels);
        for(unsigned int i=0;i<connectL.size()/2;i++)
        {
            const T a=connectL[2*i], b=connectL[2*i+1];

            if(connectivity.getValue()==1)
            {
                bimg_forT(img,t)
                {
#ifdef _OPENMP
#pragma omp parallel for
#endif
                    bimg_foroff1D(img,off1D)
                            for( unsigned va=0 ; va<img.imgList[t][off1D].size() ; ++va )
                            if(img(off1D,va,0,t)==a)
                            for( unsigned vb=0 ; vb<img.imgList[t][off1D].size() ; ++vb )
                            if(img(off1D,vb,0,t)==b)
                    {
                        img.imgList[t][off1D][va].addNeighbour(VoxelIndex(off1D,vb));
                        img.imgList[t][off1D][vb].addNeighbour(VoxelIndex(off1D,va));
                    }

                }
            }
            else if(connectivity.getValue()==7)
            {
                bimg_forT(img,t)
                {
                    //#ifdef _OPENMP
                    //#pragma omp parallel for
                    //#endif
                    bimg_foroff1D(img,off1D)
                            for( unsigned va=0 ; va<img.imgList[t][off1D].size() ; ++va )
                            if(img(off1D,va,0,t)==a)
                    {
                        // central voxel
                        for( unsigned vb=0 ; vb<img.imgList[t][off1D].size() ; ++vb )
                            if(img(off1D,vb,0,t)==b)
                            {
                                img.imgList[t][off1D][va].addNeighbour(VoxelIndex(off1D,vb));
                                img.imgList[t][off1D][vb].addNeighbour(VoxelIndex(off1D,va));
                            }
                        // 6 neighbors
                        unsigned x,y,z; img.index1Dto3D(off1D,x,y,z);
                        for( int delta = -1 ; delta <= 1 ; delta+=2 )
                            for( unsigned d = 0 ; d < 3 ; ++d )
                            {
                                int g[3]={0,0,0}; g[d]=delta;
                                if(img.isInside((int)x+g[0],(int)y+g[1],(int)z+g[2]))
                                {
                                    unsigned n = img.index3Dto1D((int)x+g[0],(int)y+g[1],(int)z+g[2]);
                                    for( unsigned vb=0 ; vb<img.imgList[t][n].size() ; ++vb )
                                        if(img(n,vb,0,t)==b)
                                        {
                                            img.imgList[t][off1D][va].addNeighbour(VoxelIndex(n,vb));
                                            img.imgList[t][n][vb].addNeighbour(VoxelIndex(off1D,va));
                                        }
                                }
                            }
                    }
                }
            }
            else
            {
                bimg_forT(img,t)
                {
                    //#ifdef _OPENMP
                    //#pragma omp parallel for
                    //#endif
                    bimg_foroff1D(img,off1D)
                            for( unsigned va=0 ; va<img.imgList[t][off1D].size() ; ++va )
                            if(img(off1D,va,0,t)==a)
                    {
                        unsigned x,y,z; img.index1Dto3D(off1D,x,y,z);
                        for( int gx = -1 ; gx <= 1 ; ++gx )
                            for( int gy = -1 ; gy <= 1 ; ++gy )
                                for( int gz = -1 ; gz <= 1 ; ++gz )
                                    if(img.isInside((int)x+gx,(int)y+gy,(int)z+gz))
                                    {
                                        unsigned n = img.index3Dto1D((int)x+gx,(int)y+gy,(int)z+gz);
                                        for( unsigned vb=0 ; vb<img.imgList[t][n].size() ; ++vb )
                                            if(img(n,vb,0,t)==b)
                                            {
                                                img.imgList[t][off1D][va].addNeighbour(VoxelIndex(n,vb));
                                                img.imgList[t][n][vb].addNeighbour(VoxelIndex(off1D,va));
                                            }
                                    }
                    }
                }
            }



        }

        sout << "Created merged image from " << nb << " input images." << sendl;

    }

    defaulttype::Vec<2,Coord> getBB(unsigned int i) // get image corners
    {
        defaulttype::Vec<2,Coord> BB;
        raImage rimage(this->inputImages[i]);
        raTransform rtransform(this->inputTransforms[i]);

        const imCoord dim= rimage->getDimensions();
        defaulttype::Vec<8,Coord> p;
        p[0]=defaulttype::Vector3(0,0,0);
        p[1]=defaulttype::Vector3(dim[0]-1,0,0);
        p[2]=defaulttype::Vector3(0,dim[1]-1,0);
        p[3]=defaulttype::Vector3(dim[0]-1,dim[1]-1,0);
        p[4]=defaulttype::Vector3(0,0,dim[2]-1);
        p[5]=defaulttype::Vector3(dim[0]-1,0,dim[2]-1);
        p[6]=defaulttype::Vector3(0,dim[1]-1,dim[2]-1);
        p[7]=defaulttype::Vector3(dim[0]-1,dim[1]-1,dim[2]-1);

        Coord tp=rtransform->fromImage(p[0]);
        BB[0]=tp;
        BB[1]=tp;
        for(unsigned int j=1; j<8; j++)
        {
            tp=rtransform->fromImage(p[j]);
            for(unsigned int k=0; k<tp.size(); k++)
            {
                if(BB[0][k]>tp[k]) BB[0][k]=tp[k];
                if(BB[1][k]<tp[k]) BB[1][k]=tp[k];
            }
        }
        return BB;
    }

    const Coord getScale(unsigned int i) const
    {
        raTransform rtransform(this->inputTransforms[i]);
        return rtransform->getScale();
    }
};


} // namespace engine

} // namespace component

} // namespace sofa

#endif // SOFA_BRANCHINGIMAGE_MergeBranchingImages_H
