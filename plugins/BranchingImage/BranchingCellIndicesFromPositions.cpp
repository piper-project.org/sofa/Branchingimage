#define SOFA_BRANCHINGIMAGE_BranchingCellIndicesFromPositions_CPP

#include "BranchingCellIndicesFromPositions.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(BranchingCellIndicesFromPositions)

int BranchingCellIndicesFromPositionsClass = core::RegisterObject("Returns global index of branching image voxels at sample locations, given a fine image of superimposed offsets")
        .add<BranchingCellIndicesFromPositions<ImageUI,BranchingImageUC> >(true)
        .add<BranchingCellIndicesFromPositions<ImageUI,BranchingImageB> >()
        .add<BranchingCellIndicesFromPositions<ImageUI,BranchingImageD> >()
        .add<BranchingCellIndicesFromPositions<ImageUI,BranchingImageUS> >()
        ;

template class SOFA_BRANCHINGIMAGE_API BranchingCellIndicesFromPositions<ImageUI,BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellIndicesFromPositions<ImageUI,BranchingImageB>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellIndicesFromPositions<ImageUI,BranchingImageD>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellIndicesFromPositions<ImageUI,BranchingImageUS>;

} //
} // namespace component
} // namespace sofa

