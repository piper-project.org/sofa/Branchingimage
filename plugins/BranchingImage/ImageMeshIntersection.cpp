#define SOFA_BRANCHINGIMAGE_ImageMeshIntersection_CPP

#include "ImageMeshIntersection.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(ImageMeshIntersection)

int ImageMeshIntersectionClass = core::RegisterObject("Returns the index of the mesh cell containing each non empty voxel")
        .add<ImageMeshIntersection<BranchingImageUC> >(true)
        .add<ImageMeshIntersection<BranchingImageUS> >()
        .add<ImageMeshIntersection<ImageUC> >()
        .add<ImageMeshIntersection<ImageUS> >()
        ;

template class SOFA_BRANCHINGIMAGE_API ImageMeshIntersection<BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API ImageMeshIntersection<BranchingImageUS>;
template class SOFA_BRANCHINGIMAGE_API ImageMeshIntersection<ImageUC>;
template class SOFA_BRANCHINGIMAGE_API ImageMeshIntersection<ImageUS>;

} //
} // namespace component
} // namespace sofa

