#ifndef SOFA_BRANCHINGIMAGE_MergeImagesIntoBranching_H
#define SOFA_BRANCHINGIMAGE_MergeImagesIntoBranching_H

#include <BranchingImage/config.h>
#include "BranchingImage.h"
#include <sofa/core/DataEngine.h>
#include <sofa/core/objectmodel/BaseObject.h>
#include <sofa/defaulttype/Vec.h>
#include <sofa/helper/vectorData.h>

//#ifdef _OPENMP
//#include <omp.h>
//#endif

namespace sofa
{

namespace component
{

namespace engine
{


/**
 * This class makes a branching image from regular Images
 */


template <class _InImageTypes,class _OutImageTypes>
class MergeImagesIntoBranching : public core::DataEngine
{
public:
    typedef core::DataEngine Inherited;
    SOFA_CLASS(SOFA_TEMPLATE2(MergeImagesIntoBranching,_InImageTypes,_OutImageTypes),Inherited);

    typedef _InImageTypes InImageTypes;
    typedef typename InImageTypes::T Ti;
    typedef typename InImageTypes::imCoord imCoordi;
    typedef helper::ReadAccessor<Data< InImageTypes > > raImagei;

    typedef _OutImageTypes OutImageTypes;
    typedef typename OutImageTypes::T To;
    typedef typename OutImageTypes::imCoord imCoordo;
    typedef helper::WriteOnlyAccessor<Data< OutImageTypes > > waImageo;

    typedef SReal Real;
    typedef defaulttype::ImageLPTransform<Real> TransformType;
    typedef typename TransformType::Coord Coord;
    typedef helper::WriteOnlyAccessor<Data< TransformType > > waTransform;
    typedef helper::ReadAccessor<Data< TransformType > > raTransform;

    Data<unsigned int> nbImages;

    helper::vectorData<InImageTypes> inputImages;
    helper::vectorData<TransformType> inputTransforms;

    Data<OutImageTypes> image;
    Data<TransformType> transform;

    Data<helper::vector<Ti> > connectLabels;
    Data<unsigned> connectivity;

    virtual std::string getTemplateName() const    { return templateName(this);    }
    static std::string templateName(const MergeImagesIntoBranching<InImageTypes,OutImageTypes>* = NULL) { return InImageTypes::Name()+std::string(",")+OutImageTypes::Name(); }

    MergeImagesIntoBranching()    :   Inherited()
      , nbImages ( initData ( &nbImages,(unsigned int)0,"nbImages","number of images to merge" ) )
      , inputImages(this, "image", "input image", helper::DataEngineInput)
      , inputTransforms(this, "transform", "input transform", helper::DataEngineInput)
      , image(initData(&image,OutImageTypes(),"image","Image"))
      , transform(initData(&transform,TransformType(),"transform","Transform"))
      , connectLabels(initData(&connectLabels,"connectLabels","Pairs of label to be connected accross different input images"))
      , connectivity(initData(&connectivity,(unsigned)27,"connectivity","must be 1, 7 or 27 (27 by default or any incorrect value)"))
    {
        inputImages.resize(nbImages.getValue());
        inputTransforms.resize(nbImages.getValue());
        image.setReadOnly(true);
        transform.setReadOnly(true);
        this->addAlias(&image, "outputImage");
        this->addAlias(&image, "branchingImage");
        this->addAlias(&transform, "outputTransform");
        this->addAlias(&transform, "branchingTransform");
    }

    virtual ~MergeImagesIntoBranching()
    {

    }

    virtual void init()
    {
        addInput(&nbImages);
        inputImages.resize(nbImages.getValue());
        inputTransforms.resize(nbImages.getValue());
        addInput(&connectLabels);
        addInput(&connectivity);

        addOutput(&image);
        addOutput(&transform);

        setDirtyValue();
    }

    virtual void reinit()
    {
        inputImages.resize(nbImages.getValue());
        inputTransforms.resize(nbImages.getValue());
        update();
    }


    /// Parse the given description to assign values to this object's fields and potentially other parameters
    void parse ( sofa::core::objectmodel::BaseObjectDescription* arg )
    {
        inputImages.parseSizeData(arg, nbImages);
        inputTransforms.parseSizeData(arg, nbImages);
        Inherit1::parse(arg);
    }

    /// Assign the field values stored in the given map of name -> value pairs
    void parseFields ( const std::map<std::string,std::string*>& str )
    {
        inputImages.parseFieldsSizeData(str, nbImages);
        inputTransforms.parseFieldsSizeData(str, nbImages);
        Inherit1::parseFields(str);
    }


protected:

    // returns if two voxels are connected given their position, if they come from the same regular image, and if their labels are in the connection list
    inline bool connected(const Coord& p1, const  Coord& p2, const bool sameImage, const bool labelConnected)
    {
        if(sameImage)
        {
            if(p1==p2) return false;// same voxel
            if(connectivity.getValue()>7) return true; // only 26 neighbors are checked
            if((p1-p2).norm2()==1) return true; // 6 neighbors
            return false;
        }

        else if(connectivity.getValue()==1)
        {
            if(p1!=p2) return false; // different voxel
            if(labelConnected) return true;
            return false;
        }
        else if(connectivity.getValue()==7)
        {
            if((p1-p2).norm2()>1) return false; // not in neighborhood
            if(labelConnected) return true;
            return false;
        }

        if(labelConnected) return true;
        return false;
    }

    virtual void update()
    {
        updateAllInputsIfDirty();
        cleanDirty();

        unsigned int nb = nbImages.getValue();
        if(!nb) return;

        // init BB
        defaulttype::Vec<2,Coord> BB = this->getBB(0);
        Coord minScale = this->getScale(0);
        for(unsigned int j=1; j<nb; j++)
        {
            defaulttype::Vec<2,Coord> bb = this->getBB(j);
            for(unsigned int k=0; k<bb[0].size(); k++)
            {
                if(BB[0][k]>bb[0][k]) BB[0][k]=bb[0][k];
                if(BB[1][k]<bb[1][k]) BB[1][k]=bb[1][k];
            }
            for(unsigned int k=0; k<3; k++)
                if( minScale[k] > this->getScale(j)[k] )
                    minScale[k] = this->getScale(j)[k];
        }

        // get input Data
        helper::vector<const InImageTypes*> in;
        helper::vector<const TransformType*> inT;
        for(unsigned int j=0; j<nb; j++)
        {
            raImagei inData(this->inputImages[j]);  in.push_back(&inData.ref());
            if(in.back()->isEmpty())  { this->serr<<"Image "<<j<<" not found"<<this->sendl; return; }
            raTransform inTData(this->inputTransforms[j]);   inT.push_back(&inTData.ref());
        }

        // init transform = translated version of inputTransforms[0] with minimum voxel size
        waTransform outT(this->transform);
        outT->operator=(*(inT[0]));
        outT->getRotation()=Coord(); //reset rotation because output image is axis aligned
        outT->getTranslation()=BB[0];
        outT->getScale()=minScale;

        // init image
        imCoordi dim=in[0]->getDimensions();
        Coord MaxP=outT->toImage(BB[1]); // corner pixel = dim-1
        dim[InImageTypes::DIMENSION_X]=ceil(MaxP[0])+1;
        dim[InImageTypes::DIMENSION_Y]=ceil(MaxP[1])+1;
        dim[InImageTypes::DIMENSION_Z]=ceil(MaxP[2])+1;

        waImageo outData(this->image);  OutImageTypes& img = outData.wref();
        img.setDimensions(dim);

        // fill image and record index to input data
        typedef typename OutImageTypes::VoxelIndex VoxelIndex;
        typedef typename OutImageTypes::ConnectionVoxel ConnectionVoxel;
//        typedef typename OutImageTypes::Neighbours Neighbours;
        //        typedef typename OutImageTypes::SuperimposedVoxels SuperimposedVoxels;

        typedef std::pair<Coord,unsigned> CoordOffsetPair;
        std::vector< std::map<CoordOffsetPair,unsigned> > branchingToImageMap((size_t)dim[OutImageTypes::DIMENSION_T]);

        //#ifdef _OPENMP
        //#pragma omp parallel for
        //#endif
        bimg_forXYZ(img,x,y,z)
        {
            CoordOffsetPair CoordOffset(Coord(x,y,z),0);
            unsigned index1d = img.index3Dto1D(x,y,z);
            Coord p = outT->fromImage(CoordOffset.first); //coordinate of voxel (x,y,z) in world space
            for(unsigned int j=0; j<nb; j++)
            {
                Coord P=inT[j]->toImageInt(p); //corresponding voxel in image j
                if(in[j]->isInside((int)P[0],(int)P[1],(int)P[2]))
                {
                    for(unsigned int t=0; t<dim[OutImageTypes::DIMENSION_T]; t++) // time
                    {
                        ConnectionVoxel v(dim[OutImageTypes::DIMENSION_S]);
                        bool isEmpty=true;
                        for(unsigned int k=0; k<dim[OutImageTypes::DIMENSION_S]; k++) // channels
                        {
                            v[k]=(To)in[j]->getCImg(j)((int)P[0],(int)P[1],(int)P[2],k);
                            if(v[k]!=(To)0)  isEmpty=false;
                        }
                        if(!isEmpty)
                        {
                            CoordOffset.second=img.imgList[t][index1d].size();
                            img.imgList[t][index1d].push_back( v , dim[OutImageTypes::DIMENSION_S]);
                            branchingToImageMap[t][CoordOffset] = j;
                        }
                    }
                }
            }
        }

        // precompute set to speed up checking
        helper::ReadAccessor<Data< helper::vector<Ti> > > connectL(this->connectLabels);
        typedef std::pair<To,To>  ToPair;
        std::set<ToPair> connectS;
        for(unsigned int i=0;i<connectL.size()/2;i++)
        {
            connectS.insert(ToPair(connectL[2*i],connectL[2*i+1]));
            connectS.insert(ToPair(connectL[2*i+1],connectL[2*i]));
        }

        // connections:  simple but unoptimized since connections are checked twice
        bimg_forT(img,t)  // time
        {
            bimg_forXYZ(img,x,y,z)
            {
                CoordOffsetPair CoordOffset1(Coord(x,y,z),0);
                unsigned index1d1 = img.index3Dto1D(x,y,z);

                for( CoordOffset1.second=0 ; CoordOffset1.second<img.imgList[t][index1d1].size() ; ++CoordOffset1.second )
                    for( int gx = -1 ; gx <= 1 ; ++gx )
                        for( int gy = -1 ; gy <= 1 ; ++gy )
                            for( int gz = -1 ; gz <= 1 ; ++gz )
                                if(img.isInside((int)x+gx,(int)y+gy,(int)z+gz))
                                {
                                    CoordOffsetPair CoordOffset2(Coord((int)x+gx,(int)y+gy,(int)z+gz),0);
                                    unsigned index1d2 = img.index3Dto1D((int)x+gx,(int)y+gy,(int)z+gz);

                                    for( CoordOffset2.second=0 ; CoordOffset2.second<img.imgList[t][index1d2].size() ; ++CoordOffset2.second )
                                    {
                                        bool labelConnected= connectS.find(ToPair(img(index1d1,CoordOffset1.second,0,t),img(index1d2,CoordOffset2.second,0,t)))==connectS.end()?false:true;
                                        bool sameImage= branchingToImageMap[t][CoordOffset1]==branchingToImageMap[t][CoordOffset2]?true:false;
                                        if(connected(CoordOffset1.first,CoordOffset2.first,sameImage,labelConnected))
                                        {
                                            img.imgList[t][index1d1][CoordOffset1.second].addNeighbour(VoxelIndex(index1d2,CoordOffset2.second));
                                            img.imgList[t][index1d2][CoordOffset2.second].addNeighbour(VoxelIndex(index1d1,CoordOffset1.second));
                                        }
                                    }
                                }
            }
        }

        // merge neighbors of connected superimposed voxels
//        bimg_forVoffT(img,offset,index1d,t)
//        {
//            const Neighbours& neighbours = img.imgList[t][index1d][offset].neighbours;
//            for( unsigned n = 0 ; n < neighbours.size() ; ++n )
//                if(neighbours[n].index1d==index1d && neighbours[n].offset!=offset ) // check if neighbor is a connected superimposed voxels
//                    for( unsigned n2 = 0 ; n2 < neighbours.size() ; ++n2 ) // add my neighbors
//                        if(n2!=n)
//                            img.imgList[t][neighbours[n].index1d][neighbours[n].offset].addNeighbour( neighbours[n2] );
//        }

        sout << "Created merged image from " << nb << " input images." << sendl;
    }

    defaulttype::Vec<2,Coord> getBB(unsigned int i) // get image corners
    {
        defaulttype::Vec<2,Coord> BB;
        raImagei rimage(this->inputImages[i]);
        raTransform rtransform(this->inputTransforms[i]);

        const imCoordi dim= rimage->getDimensions();
        defaulttype::Vec<8,Coord> p;
        p[0]=defaulttype::Vector3(0,0,0);
        p[1]=defaulttype::Vector3(dim[0]-1,0,0);
        p[2]=defaulttype::Vector3(0,dim[1]-1,0);
        p[3]=defaulttype::Vector3(dim[0]-1,dim[1]-1,0);
        p[4]=defaulttype::Vector3(0,0,dim[2]-1);
        p[5]=defaulttype::Vector3(dim[0]-1,0,dim[2]-1);
        p[6]=defaulttype::Vector3(0,dim[1]-1,dim[2]-1);
        p[7]=defaulttype::Vector3(dim[0]-1,dim[1]-1,dim[2]-1);

        Coord tp=rtransform->fromImage(p[0]);
        BB[0]=tp;
        BB[1]=tp;
        for(unsigned int j=1; j<8; j++)
        {
            tp=rtransform->fromImage(p[j]);
            for(unsigned int k=0; k<tp.size(); k++)
            {
                if(BB[0][k]>tp[k]) BB[0][k]=tp[k];
                if(BB[1][k]<tp[k]) BB[1][k]=tp[k];
            }
        }
        return BB;
    }

    const Coord getScale(unsigned int i) const
    {
        raTransform rtransform(this->inputTransforms[i]);
        return rtransform->getScale();
    }
};


} // namespace engine

} // namespace component

} // namespace sofa

#endif // SOFA_BRANCHINGIMAGE_MergeImagesIntoBranching_H
