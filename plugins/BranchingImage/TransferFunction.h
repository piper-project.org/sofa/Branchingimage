#ifndef SOFA_BRANCHINGIMAGE_TRANSFERFUNCTION_H
#define SOFA_BRANCHINGIMAGE_TRANSFERFUNCTION_H

#include <BranchingImage/config.h>
#include <image/TransferFunction.h>
#include "BranchingImage.h"


namespace sofa
{
namespace component
{
namespace engine
{

/**
 * This class transforms pixel intensities
 */


/// Specialization for branching Image
template <class Ti, class To>
struct TransferFunctionSpecialization<defaulttype::BranchingImage<Ti>,defaulttype::BranchingImage<To>>
{
    typedef TransferFunction<defaulttype::BranchingImage<Ti>,defaulttype::BranchingImage<To>> TransferFunctionT;

    static void update(TransferFunctionT& This)
    {
        typename TransferFunctionT::raParam p(This.param);
        typename TransferFunctionT::raImagei in(This.inputImage);
        if(in->isEmpty()) return;
        const typename TransferFunctionT::InImageTypes& inimg = in.ref();

        typename TransferFunctionT::waImageo out(This.outputImage);
        typename TransferFunctionT::imCoord dim=in->getDimensions();
        typename TransferFunctionT::OutImageTypes& img = out.wref();
        img.setDimensions(dim);
        img.cloneTopology (inimg,0);

        switch(This.filter.getValue().getSelectedId())
        {
        case LINEAR:
        {
            typename TransferFunctionT::iomap mp; for(unsigned int i=0; i<p.size(); i+=2) mp[(Ti)p[i]]=(To)p[i+1];
            bimg_forCVoffT(inimg,c,v,off1D,t) img(off1D,v,c,t)=This.Linear_TransferFunction(inimg(off1D,v,c,t),mp);
        }
            break;

        default:
            bimg_forCVoffT(inimg,c,v,off1D,t) img(off1D,v,c,t)=(Ti)inimg(off1D,v,c,t); // copy
            break;
        }
    }

};



} // namespace engine
} // namespace component
} // namespace sofa

#endif // SOFA_BRANCHINGIMAGE_TRANSFERFUNCTION_H
