#define SOFA_BRANCHINGIMAGE_BranchingCellOffsetsFromConnectLabels_CPP

#include "BranchingCellOffsetsFromConnectLabels.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(BranchingCellOffsetsFromConnectLabels)

int BranchingCellOffsetsFromConnectLabelsClass = core::RegisterObject("Returns offsets of superimposed voxels at positions where point labels and image labels overlap")
        .add<BranchingCellOffsetsFromConnectLabels<BranchingImageUC> >(true)
        .add<BranchingCellOffsetsFromConnectLabels<BranchingImageUS> >()
        ;

template class SOFA_BRANCHINGIMAGE_API BranchingCellOffsetsFromConnectLabels<BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API BranchingCellOffsetsFromConnectLabels<BranchingImageUS>;

} //
} // namespace component
} // namespace sofa

