#ifndef BRANCHINGIMAGE_FLEXIBLE_VoronoiShapeFunction_H
#define BRANCHINGIMAGE_FLEXIBLE_VoronoiShapeFunction_H

#include <BranchingImage/config.h>
#include <Flexible/shapeFunction/VoronoiShapeFunction.h>

#include "BaseImageShapeFunction.h"
#include <Flexible/shapeFunction/BaseShapeFunction.h>

namespace sofa
{
namespace component
{
namespace shapefunction
{

/**
Voronoi shape functions are natural neighbor interpolants
there are computed from an image (typically a rasterized object)
  */

/// Specialization for branching Image
template <class T>
struct VoronoiShapeFunctionSpecialization<defaulttype::BranchingImage<T>>
{
    template<class VoronoiShapeFunction>
    static void init(VoronoiShapeFunction* This)
    {
        typedef typename VoronoiShapeFunction::ImageTypes ImageTypes;
        typedef typename VoronoiShapeFunction::raImage raImage;
        typedef typename VoronoiShapeFunction::DistTypes DistTypes;
        typedef typename VoronoiShapeFunction::waDist waDist;
        typedef typename VoronoiShapeFunction::DistT DistT;
        typedef typename VoronoiShapeFunction::IndTypes IndTypes;
        typedef typename VoronoiShapeFunction::waInd waInd;

        // retrieve data
        raImage inData(This->image);    const ImageTypes& in = inData.ref();
        if(in.isEmpty())  { This->serr<<"Image not found"<<This->sendl; return; }

        // init voronoi and distances
        typename VoronoiShapeFunction::imCoord dim = in.getDimensions(); dim[ImageTypes::DIMENSION_S]=dim[ImageTypes::DIMENSION_T]=1;

        waInd vorData(This->f_voronoi);  IndTypes& voronoi = vorData.wref();
        voronoi.setDimensions(dim);
        voronoi.cloneTopology (in,0);

        waDist distData(This->f_distances);        DistTypes& dist = distData.wref();
        dist.setDimensions(dim);
        dist.cloneTopology (in,-1.0);
        bimg_forCVoffT(in,c,v,off1D,t) if(t==0 && c==0) if(in(off1D,v,c,t)) dist(off1D,v,c,0)=cimg_library::cimg::type<DistT>::max();

        // init indices and weights images
        unsigned int nbref=This->f_nbRef.getValue();        dim[ImageTypes::DIMENSION_S]=nbref;

        waInd indData(This->f_index); IndTypes& indices = indData.wref();
        indices.setDimensions(dim);
        indices.cloneTopology (in,0);

        waDist weightData(This->f_w);    DistTypes& weights = weightData.wref();
        weights.setDimensions(dim);
        weights.cloneTopology (in,0);
    }


    template<class VoronoiShapeFunction>
    static void computeVoronoi(VoronoiShapeFunction* This)
    {
        typedef typename VoronoiShapeFunction::ImageTypes ImageTypes;
        typedef typename VoronoiShapeFunction::Coord Coord;
        typedef typename VoronoiShapeFunction::raImage raImage;
        typedef typename VoronoiShapeFunction::raTransform raTransform;
        typedef typename VoronoiShapeFunction::DistTypes DistTypes;
        typedef typename VoronoiShapeFunction::DistT DistT;
        typedef typename VoronoiShapeFunction::waDist waDist;
        typedef typename VoronoiShapeFunction::IndTypes IndTypes;
        typedef typename VoronoiShapeFunction::waInd waInd;

        typedef typename ImageTypes::VoxelIndex VoxelIndex;
        typedef std::pair<DistT,VoxelIndex > DistanceToPoint;

        // retrieve data
        raImage inData(This->image);    const ImageTypes& in = inData.ref();
        raTransform inT(This->transform);
        if(in.isEmpty())  { This->serr<<"Image not found"<<This->sendl; return; }
        const ImageTypes* biasFactor=This->biasDistances.getValue()?&in:NULL;

        waInd vorData(This->f_voronoi);            IndTypes& voronoi = vorData.wref();
        waDist distData(This->f_distances);        DistTypes& dist = distData.wref();

        helper::ReadAccessor<Data<helper::vector<Coord> > > parent(This->f_position);
        if(!parent.size()) { This->serr<<"Parent nodes not found"<<This->sendl; return; }
        helper::vector<VoxelIndex> parentiCoord;
        for(unsigned int i=0; i<parent.size(); i++)
        {
            Coord p = inT->toImageInt(parent[i]);
            parentiCoord.push_back(VoxelIndex(in.index3Dto1D(p[0],p[1],p[2]),0));
            if(This->f_cell.getValue().size()>i) if(This->f_cell.getValue()[i]>0) parentiCoord.back().offset=This->f_cell.getValue()[i]-1;
        }

        // compute voronoi and distances based on nodes
        std::set<DistanceToPoint> trial;                // list of seed points
        for(unsigned int i=0; i<parent.size(); i++)            AddSeedPoint<DistT>(trial,dist,voronoi, parentiCoord[i],i+1);

        if(This->useDijkstra.getValue()) dijkstra<DistT,T>(trial,dist, voronoi, inT->getScale() , biasFactor); else fastMarching<DistT,T>(trial,dist, voronoi, inT->getScale() ,biasFactor );
    }


    template<class VoronoiShapeFunction>
    static void ComputeWeigths_DistanceRatio(VoronoiShapeFunction* This)
    {
        typedef typename VoronoiShapeFunction::ImageTypes ImageTypes;
        typedef typename VoronoiShapeFunction::Coord Coord;
        typedef typename VoronoiShapeFunction::raImage raImage;
        typedef typename VoronoiShapeFunction::raTransform raTransform;
        typedef typename VoronoiShapeFunction::DistTypes DistTypes;
        typedef typename VoronoiShapeFunction::DistT DistT;
        typedef typename VoronoiShapeFunction::waDist waDist;
        typedef typename VoronoiShapeFunction::IndTypes IndTypes;
        typedef typename VoronoiShapeFunction::waInd waInd;

        typedef typename ImageTypes::VoxelIndex VoxelIndex;
        typedef typename ImageTypes::Neighbours Neighbours;
        typedef std::pair<DistT,VoxelIndex > DistanceToPoint;

        // retrieve data
        raImage inData(This->image);    const ImageTypes& in = inData.ref();
        raTransform inT(This->transform);
        if(in.isEmpty())  { This->serr<<"Image not found"<<This->sendl; return; }
        const ImageTypes* biasFactor=This->biasDistances.getValue()?&in:NULL;

        waInd vorData(This->f_voronoi);            IndTypes& voronoi = vorData.wref();
        waDist distData(This->f_distances);        DistTypes& dist = distData.wref();
        waInd indData(This->f_index);              IndTypes& indices = indData.wref();
        waDist weightData(This->f_w);              DistTypes& weights = weightData.wref();

        helper::ReadAccessor<Data<helper::vector<Coord> > > parent(This->f_position);
        if(!parent.size()) { This->serr<<"Parent nodes not found"<<This->sendl; return; }
        helper::vector<VoxelIndex> parentiCoord;
        for(unsigned int i=0; i<parent.size(); i++)
        {
            Coord p = inT->toImageInt(parent[i]);
            parentiCoord.push_back(VoxelIndex(in.index3Dto1D(p[0],p[1],p[2]),0));
            if(This->f_cell.getValue().size()>i) if(This->f_cell.getValue()[i]>0) parentiCoord.back().offset=This->f_cell.getValue()[i]-1;
        }

        unsigned int nbref=This->f_nbRef.getValue();

        // compute weight of each parent
        for(unsigned int i=0; i<parentiCoord.size(); i++)
        {
            std::set<DistanceToPoint> trial;                // list of seed points

            // distance max to voronoi
            DistT dmax=0;
            bimg_forCVoffT(voronoi,c,v,off1D,t) if(voronoi(off1D,v,c,t)==i+1)
            {
                if(dmax<dist(off1D,v,c,t)) dmax=dist(off1D,v,c,t);
                // check neighbors to retrieve upper distance bound
                Neighbours neighbours = voronoi.getNeighbours(VoxelIndex(off1D,v));
                for (unsigned int n=0; n<neighbours.size(); n++)    if(voronoi(neighbours[n],c,t)!=i+1) if(voronoi(neighbours[n],c,t)!=0)  if(dmax<dist(neighbours[n],c,t)) dmax=dist(neighbours[n],c,t);
            }

            // extend voronoi to 2*dmax
            DistTypes distP(dist,false);  bimg_forCVoffT(distP,c,v,off1D,t) if(distP(off1D,v,c,t)!=-1)  distP(off1D,v,c,t)=dmax*(DistT)2.;
            IndTypes voronoiP(voronoi,false);
            AddSeedPoint<DistT>(trial,distP,voronoiP, parentiCoord[i],i+1);
            if(This->useDijkstra.getValue()) dijkstra<DistT,T>(trial,distP, voronoiP, inT->getScale() , biasFactor);            else fastMarching<DistT,T>(trial,distP, voronoiP, inT->getScale() ,biasFactor );

            // distances from voronoi border
            DistTypes distB(dist,false);  bimg_forCVoffT(distB,c,v,off1D,t) if(distB(off1D,v,c,t)!=-1)  distB(off1D,v,c,t)=dmax;
            IndTypes voronoiB(voronoi,false);
            bimg_forCVoffT(voronoi,c,v,off1D,t) if(voronoi(off1D,v,c,t)==i+1)
            {
                Neighbours neighbours = voronoi.getNeighbours(VoxelIndex(off1D,v));
                bool border=false;
                for (unsigned int n=0; n<neighbours.size(); n++)
                    //if(voronoi.getDirection( off1D,neighbours[n].index1d ).connectionType()==NeighbourOffset::FACE)
                    if(voronoi(neighbours[n],c,t)!=i+1) if(voronoi(neighbours[n],c,t)!=0)
                    {
                        border=true;
                        // subpixel voronoi frontier localization
                        distB(neighbours[n],c,t)= (DistT)0.5*( distP(neighbours[n],c,t) - dist(neighbours[n],c,t) );                         trial.insert( DistanceToPoint(distB(neighbours[n],c,t),neighbours[n] ) );
                        DistT d = (DistT)0.5*(dist(neighbours[n],c,t) + distP(neighbours[n],c,t)) - dist(off1D,v,c,t);
                        if(d<0) d=0;
                        if(d<distB(off1D,v,c,t)) distB(off1D,v,c,t) = d;
                    }
                if(border) trial.insert( DistanceToPoint(distB(off1D,v,c,t),VoxelIndex(off1D,v) ) );
            }
            if(This->useDijkstra.getValue()) dijkstra<DistT,T>(trial,distB, voronoiB, inT->getScale() , biasFactor);            else fastMarching<DistT,T>(trial,distB, voronoiB, inT->getScale() ,biasFactor );

            // compute weight as distance ratio
            DistT TOL = 1E-4; // warning: hard coded tolerance on the weights (to maximize sparsity)
            bimg_forCVoffT(voronoiP,c,v,off1D,t) if(voronoiP(off1D,v,c,t)==i+1)
            {
                DistT w;
                DistT db=distB(off1D,v,c,t),dp=distP(off1D,v,c,t);

                if(dp==0) w=(DistT)1.;
                else if(voronoi(off1D,v,c,t)==i+1) w=(DistT)0.5*((DistT)1. + db/(dp+db)); // inside voronoi: dist(frame,closestVoronoiBorder)=d+disttovoronoi
                else if(dp==db) w=(DistT)0.;
                else w=(DistT)0.5*((DistT)1. - db/(dp-db)); // outside voronoi: dist(frame,closestVoronoiBorder)=d-disttovoronoi
                if(w<TOL) w=0; else if(w>(DistT)1.-TOL) w=(DistT)1.;

                // insert in weights
                unsigned int j=0;
                while(j!=nbref && weights(off1D,v,j,t)>=w) j++;
                if(j!=nbref)
                {
                    if(j!=nbref-1) for(unsigned int k=nbref-1; k>j; k--) { weights(off1D,v,k,t)=weights(off1D,v,k-1,t); indices(off1D,v,k,t)=indices(off1D,v,k-1,t); }
                    weights(off1D,v,j,t)=w;
                    indices(off1D,v,j,t)=i+1;
                }
            }
        }
        // normalize
        bimg_forCVoffT(voronoi,c,v,off1D,t) if(voronoi(off1D,v,c,t))
        {
            DistT totW=0;
            bimg_forC(weights,c) totW+=weights(off1D,v,c,t);
            if(totW) bimg_forC(weights,c) weights(off1D,v,c,t)/=totW;
        }
    }


    /**
    * returns Natural Neighbor Interpolant coordinates of a point @param index: http://dilbert.engr.ucdavis.edu/~suku/nem/
    * from :
    *   - initial nodal voronoi regions (@param voronoi and @param distances)
    *   - the updated voronoi including the point (@param voronoiPt and @param distancesPt)
    * returns volume, area and distance associated to each natural neighbor (indexed in @param ref)
    */

    template<class VoronoiShapeFunction>
    static void ComputeWeigths_NaturalNeighbors(VoronoiShapeFunction* This)
    {
        typedef typename VoronoiShapeFunction::ImageTypes ImageTypes;
        typedef typename VoronoiShapeFunction::Real Real;
        typedef typename VoronoiShapeFunction::Coord Coord;
        typedef typename VoronoiShapeFunction::raImage raImage;
        typedef typename VoronoiShapeFunction::raTransform raTransform;
        typedef typename VoronoiShapeFunction::DistTypes DistTypes;
        typedef typename VoronoiShapeFunction::DistT DistT;
        typedef typename VoronoiShapeFunction::waDist waDist;
        typedef typename VoronoiShapeFunction::IndTypes IndTypes;
        typedef typename VoronoiShapeFunction::waInd waInd;

        typedef typename ImageTypes::VoxelIndex VoxelIndex;
        typedef typename ImageTypes::Neighbours Neighbours;
        typedef typename ImageTypes::NeighbourOffset NeighbourOffset;
        typedef std::pair<DistT,VoxelIndex > DistanceToPoint;

        typedef NaturalNeighborData<Real> NNData;
        typedef typename NNData::Map NNMap;

        // retrieve data
        raImage inData(This->image);    const ImageTypes& in = inData.ref();
        raTransform inT(This->transform);
        if(in.isEmpty())  { This->serr<<"Image not found"<<This->sendl; return; }
        const ImageTypes* biasFactor=This->biasDistances.getValue()?&in:NULL;

        waInd vorData(This->f_voronoi);            IndTypes& voronoi = vorData.wref();
        waDist distData(This->f_distances);        DistTypes& dist = distData.wref();
        waInd indData(This->f_index);              IndTypes& indices = indData.wref();
        waDist weightData(This->f_w);              DistTypes& weights = weightData.wref();

        unsigned int nbref=This->f_nbRef.getValue();

        Coord voxelsize(inT->getScale());
        Real pixelvol=voxelsize[0]*voxelsize[1]*voxelsize[2];
        defaulttype::Vec<3,Real> pixelsurf(voxelsize[1]*voxelsize[2],voxelsize[0]*voxelsize[2],voxelsize[0]*voxelsize[1]);
        unsigned int indexPt=This->f_position.getValue().size()+1; // voronoi index of points that will be added to compute NNI

        IndTypes voronoiPt(voronoi,false);
        DistTypes distPt(dist,false);

        // compute weights voxel-by-voxel
        bimg_forCVoffT(voronoi,ci,vi,off1Di,ti) if(voronoi(off1Di,vi,ci,ti))
        {
            // compute updated voronoi including voxel (xi,yi,iz)
            std::set<DistanceToPoint> trial;                // list of seed points

            // copy
            bimg_forCVoffT(voronoiPt,c,v,off1D,t) voronoiPt(off1D,v,c,t)=voronoi(off1D,v,c,t);
            bimg_forCVoffT(distPt,c,v,off1D,t) distPt(off1D,v,c,t)=dist(off1D,v,c,t);

            AddSeedPoint<DistT>(trial,distPt,voronoiPt, VoxelIndex(off1Di,vi),indexPt);

            if(This->useDijkstra.getValue()) dijkstra<DistT,T>(trial,distPt, voronoiPt, voxelsize , biasFactor); else fastMarching<DistT,T>(trial,distPt, voronoiPt, voxelsize,biasFactor );

            // compute Natural Neighbor Data based on neighboring voronoi cells
            NNMap dat;
            bimg_forCVoffT(voronoiPt,c,v,off1D,t) if(voronoiPt(off1D,v,c,t)==indexPt)
            {
                unsigned int node=voronoi(off1D,v,c,t);
                if(!dat.count(node)) dat[node]=NNData();
                dat[node].vol+=pixelvol;
                Neighbours neighbours = voronoiPt.getNeighbours(VoxelIndex(off1D,v));
                for (unsigned int n=0; n<neighbours.size(); n++)
                    if(voronoiPt(neighbours[n],c,t)!=indexPt)
                    {
                        NeighbourOffset of = voronoiPt.getDirection( off1D,neighbours[n].index1d ) ;
                        if(of.connectionType()==NeighbourOffset::FACE)
                        {
                            if(of[0]) dat[node].surf+=pixelsurf[0];
                            else if(of[1]) dat[node].surf+=pixelsurf[1];
                            else if(of[2]) dat[node].surf+=pixelsurf[2];
                        }
                    }
                if(distPt(off1D,v,c,t)+dist(off1D,v,c,t)<dat[node].dist) dat[node].dist=distPt(off1D,v,c,t)+dist(off1D,v,c,t);
            }

            if (This->method.getValue().getSelectedId() == LAPLACE)   // replace vol (SIBSON) by surf/dist coordinates (LAPLACE)
            {
                for ( typename NNMap::iterator it=dat.begin() ; it != dat.end(); it++ )
                    if((*it).second.dist==0) (*it).second.vol=std::numeric_limits<Real>::max();
                    else (*it).second.vol=(*it).second.surf/(*it).second.dist;
            }

            // prune to nbref if necessary (nb of natural neighbors >nbref)
            while(dat.size()>nbref)
            {
                Real vmin=std::numeric_limits<Real>::max(); unsigned int key=0;
                for ( typename NNMap::iterator it=dat.begin() ; it != dat.end(); it++ ) if((*it).second.vol<vmin) key=(*it).first;
                dat.erase(key);
            }

            // compute weights based on Natural Neighbor coordinates
            Real total=0;
            for ( typename NNMap::iterator it=dat.begin() ; it != dat.end(); it++ ) total+=(*it).second.vol;
            if(total)
            {
                int count=0;
                for ( typename NNMap::iterator it=dat.begin() ; it != dat.end(); it++ )
                {
                    weights(off1Di,vi,count,ti)=(*it).second.vol/total;
                    indices(off1Di,vi,count,ti)=(*it).first;
                    count++;
                }
            }
        }
    }

};







}
}
}


#endif
