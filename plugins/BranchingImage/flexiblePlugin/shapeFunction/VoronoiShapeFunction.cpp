#define BRANCHINGIMAGE_FLEXIBLE_VoronoiShapeFunction_CPP

#include "VoronoiShapeFunction.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{
namespace component
{
namespace shapefunction
{

using namespace defaulttype;
using namespace core::behavior;

SOFA_DECL_CLASS(BranchingVoronoiShapeFunction)

// Register in the Factory
int BranchingVoronoiShapeFunctionClass = core::RegisterObject("Computes natural neighbor shape functions in images")
        .add<VoronoiShapeFunction<ShapeFunction,BranchingImageB> >()
        .add<VoronoiShapeFunction<ShapeFunction,BranchingImageUC> >()
        .add<VoronoiShapeFunction<ShapeFunction,BranchingImageD> >()
        .add<VoronoiShapeFunction<ShapeFunction,BranchingImageF> >()
        .add<VoronoiShapeFunction<ShapeFunction,BranchingImageUS> >()
        ;

template class SOFA_BRANCHINGIMAGE_API VoronoiShapeFunction<ShapeFunction,BranchingImageB>;
template class SOFA_BRANCHINGIMAGE_API VoronoiShapeFunction<ShapeFunction,BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API VoronoiShapeFunction<ShapeFunction,BranchingImageD>;
template class SOFA_BRANCHINGIMAGE_API VoronoiShapeFunction<ShapeFunction,BranchingImageF>;
template class SOFA_BRANCHINGIMAGE_API VoronoiShapeFunction<ShapeFunction,BranchingImageUS>;
}
}
}
