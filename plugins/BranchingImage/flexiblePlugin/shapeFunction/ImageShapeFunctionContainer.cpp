#define BRANCHINGIMAGE_ImageShapeFunctionContainer_CPP

#include <BranchingImage/config.h>
#include <Flexible/shapeFunction/ImageShapeFunctionContainer.h>
#include <sofa/core/ObjectFactory.h>
#include "BaseImageShapeFunction.h"

namespace sofa
{
namespace component
{
namespace shapefunction
{

using namespace defaulttype;
using namespace core::behavior;

SOFA_DECL_CLASS(BranchingImageShapeFunctionContainer)

// Register in the Factory
int BranchingImageShapeFunctionContainerClass = core::RegisterObject("Provides interface to mapping from precomputed shape functions")

        .add< ImageShapeFunctionContainer<ShapeFunction,BranchingImageUC> >()
        ;

template class SOFA_BRANCHINGIMAGE_API ImageShapeFunctionContainer<ShapeFunction,BranchingImageUC>;
}
}
}
