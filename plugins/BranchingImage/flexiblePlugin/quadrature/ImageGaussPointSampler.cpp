#define SOFA_BranchingImageGaussPointSAMPLER_CPP

#include "ImageGaussPointSampler.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{
namespace component
{
namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(BranchingImageGaussPointSampler)

// Register in the Factory
int BranchingImageGaussPointSamplerClass = core::RegisterObject("Samples an object represented by an image with gauss points")

        .add<ImageGaussPointSampler<BranchingImageD,BranchingImageUC> >()
        .add<ImageGaussPointSampler<BranchingImageF,BranchingImageUC> >()

        ;

template class SOFA_BRANCHINGIMAGE_API ImageGaussPointSampler<BranchingImageD,BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API ImageGaussPointSampler<BranchingImageF,BranchingImageUC>;


} // namespace engine
} // namespace component
} // namespace sofa

