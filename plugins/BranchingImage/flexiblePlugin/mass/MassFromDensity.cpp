#define SOFA_BranchingImage_MassFromDensity_CPP

#include <BranchingImage/config.h>
#include "MassFromDensity.h"
#include <sofa/core/ObjectFactory.h>
#include <Flexible/types/AffineTypes.h>

namespace sofa {
namespace component {
namespace engine {

using namespace defaulttype;

SOFA_DECL_CLASS(MassFromBranchingImageDensity)

int MassFromBranchingImageDensity = core::RegisterObject("Compute a mass matrix from a density image")
        .add<MassFromDensity<Affine3Types,BranchingImageD > >()
        .add<MassFromDensity<Affine3Types,BranchingImageF > >()
        .add<MassFromDensity<Affine3Types,BranchingImageUI > >()
        .add<MassFromDensity<Affine3Types,BranchingImageUC > >()
;

template class SOFA_BRANCHINGIMAGE_API MassFromDensity<Affine3Types,BranchingImageD  >;
template class SOFA_BRANCHINGIMAGE_API MassFromDensity<Affine3Types,BranchingImageF  >;
template class SOFA_BRANCHINGIMAGE_API MassFromDensity<Affine3Types,BranchingImageUI >;
template class SOFA_BRANCHINGIMAGE_API MassFromDensity<Affine3Types,BranchingImageUC >;

}
}
}
