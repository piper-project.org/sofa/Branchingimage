#define SOFA_BRANCHINGIMAGE_MeshLabeler_CPP

#include "MeshLabeler.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(MeshLabeler)

int MeshLabelerClass = core::RegisterObject("Returns image labels associated to each part of a mesh")
        .add<MeshLabeler<BranchingImageUC> >(true)
        .add<MeshLabeler<BranchingImageUS> >()
        .add<MeshLabeler<ImageUC> >()
        .add<MeshLabeler<ImageUS> >()
        ;

template class SOFA_BRANCHINGIMAGE_API MeshLabeler<BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API MeshLabeler<BranchingImageUS>;
template class SOFA_BRANCHINGIMAGE_API MeshLabeler<ImageUC>;
template class SOFA_BRANCHINGIMAGE_API MeshLabeler<ImageUS>;

} //
} // namespace component
} // namespace sofa

