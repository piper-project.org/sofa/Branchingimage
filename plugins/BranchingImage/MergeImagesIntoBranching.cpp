#define SOFA_BRANCHINGIMAGE_MergeImagesIntoBranching_CPP

#include "MergeImagesIntoBranching.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(MergeImagesIntoBranching)

int MergeImagesIntoBranchingClass = core::RegisterObject("Make a branching image from regular Images")
        .add<MergeImagesIntoBranching<ImageUC,BranchingImageUC> >(true)
        .add<MergeImagesIntoBranching<ImageD,BranchingImageD> >()
#ifdef BUILD_ALL_IMAGE_TYPES
        .add<MergeImagesIntoBranching<ImageC,BranchingImageC> >()
        .add<MergeImagesIntoBranching<ImageI,BranchingImageI> >()
        .add<MergeImagesIntoBranching<ImageUI,BranchingImageUI> >()
        .add<MergeImagesIntoBranching<ImageS,BranchingImageS> >()
        .add<MergeImagesIntoBranching<ImageUS,BranchingImageUS> >()
        .add<MergeImagesIntoBranching<ImageL,BranchingImageL> >()
        .add<MergeImagesIntoBranching<ImageUL,BranchingImageUL> >()
        .add<MergeImagesIntoBranching<ImageF,BranchingImageF> >()
        .add<MergeImagesIntoBranching<ImageB,BranchingImageB> >()
#endif
        ;

template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageUC,BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageD,BranchingImageD>;
#ifdef BUILD_ALL_IMAGE_TYPES
template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageC,BranchingImageC>;
template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageI,BranchingImageI>;
template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageUI,BranchingImageUI>;
template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageS,BranchingImageS>;
template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageUS,BranchingImageUS>;
template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageL,BranchingImageL>;
template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageUL,BranchingImageUL>;
template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageF,BranchingImageF>;
template class SOFA_BRANCHINGIMAGE_API MergeImagesIntoBranching<ImageB,BranchingImageB>;
#endif


} //
} // namespace component

} // namespace sofa

