#ifndef SOFA_BRANCHINGIMAGE_BranchingCellOffsetsFromPositions_H
#define SOFA_BRANCHINGIMAGE_BranchingCellOffsetsFromPositions_H

#include <BranchingImage/config.h>
//#include <image/ImageTypes.h>
#include "BranchingImage.h"
//#include <sofa/component/component.h>
#include <sofa/core/objectmodel/Event.h>
#include <sofa/simulation/AnimateEndEvent.h>
#include <sofa/defaulttype/Vec.h>

namespace sofa
{
namespace component
{
namespace engine
{

/**
 * Returns offsets of superimposed voxels at positions corresponding to given labels (image intensity values)
 */


template <class _BranchingImageTypes>
class BranchingCellOffsetsFromPositions : public core::DataEngine
{
public:
    typedef core::DataEngine Inherited;
    SOFA_CLASS(SOFA_TEMPLATE(BranchingCellOffsetsFromPositions,_BranchingImageTypes),Inherited);

    typedef SReal Real;

    typedef _BranchingImageTypes BranchingImageTypes;
    typedef typename BranchingImageTypes::T T;
    typedef helper::ReadAccessor<Data< BranchingImageTypes > > raBranchingImage;

    typedef helper::vector<T> labelType;
    typedef helper::ReadAccessor<Data< labelType > > raLabels;
    Data< labelType > labels;

    Data< BranchingImageTypes > branchingImage;

    typedef defaulttype::ImageLPTransform<Real> TransformType;
    typedef typename TransformType::Coord Coord;
    typedef helper::ReadAccessor<Data< TransformType > > raTransform;
    Data< TransformType > branchingImageTransform;

    typedef helper::vector<defaulttype::Vec<3,Real> > SeqPositions;
    typedef helper::ReadAccessor<Data< SeqPositions > > raPositions;
    Data< SeqPositions > position;

    Data<bool> useGlobalIndices;
    Data<bool> useIndexLabelPairs;

    typedef helper::vector<int> valuesType;
    typedef helper::WriteOnlyAccessor<Data< valuesType > > waValues;
    Data< valuesType > cell;  ///< output values

    virtual std::string getTemplateName() const    { return templateName(this);    }
    static std::string templateName(const BranchingCellOffsetsFromPositions<BranchingImageTypes>* = NULL) { return BranchingImageTypes::Name();    }

    BranchingCellOffsetsFromPositions()    :   Inherited()
      , labels(initData(&labels,"labels","labels in input image used to select superimposed voxels"))
      , branchingImage(initData(&branchingImage,BranchingImageTypes(),"branchingImage",""))
      , branchingImageTransform(initData(&branchingImageTransform,TransformType(),"branchingImageTransform",""))
      , position(initData(&position,SeqPositions(),"position","input positions"))
      , useGlobalIndices(initData(&useGlobalIndices,false,"useGlobalIndices","if true, global cell indices are computed"))
      , useIndexLabelPairs(initData(&useIndexLabelPairs,false,"useIndexLabelPairs","if true, the input 'labels' vector is a list of (point index,label) pairs, allowing to have different labels for each input position"))
      , cell( initData ( &cell,"cell","cell offsets" ) )
      , time((unsigned int)0)
    {
        this->addAlias(&branchingImageTransform, "branchingTransform");
        this->addAlias(&branchingImageTransform, "transform");
        this->addAlias(&branchingImage, "image");
        branchingImage.setReadOnly(true);
        branchingImageTransform.setReadOnly(true);
        f_listening.setValue(true);
    }

    virtual void init()
    {
        addInput(&labels);
        addInput(&branchingImage);
        addInput(&branchingImageTransform);
        addInput(&position);
        addInput(&useGlobalIndices);
        addOutput(&cell);
        setDirtyValue();
    }

    virtual void reinit() { update(); }

protected:

    unsigned int time;

    virtual void update()
    {
        raBranchingImage in(this->branchingImage);
        raTransform inT(this->branchingImageTransform);
        raPositions pos(this->position);
        raLabels rlab(this->labels);
        helper::vector<T> lab; // the used labels
        if(!useIndexLabelPairs.getValue()) // labels are the same for all points -> copy input
            lab.assign(rlab.begin(),rlab.end());
        bool glob=this->useGlobalIndices.getValue();

        cleanDirty();

        // get images at time t
        const typename BranchingImageTypes::BranchingImage3D& img = in->imgList[this->time];

        waValues val(this->cell);
        int outval=-1;
        val.resize(pos.size());

        for(unsigned int i=0; i<pos.size(); i++)
        {
             if(useIndexLabelPairs.getValue())
             {
                // labels are specific for each point -> collect them from input
                lab.clear();
                for(unsigned int l = 0 ; l<rlab.size()/2 ; l++) if((T)i==rlab[2*l]) lab.push_back(rlab[2*l+1]);
             }


            Coord Tp = inT->toImageInt(pos[i]);
            if(!in->isInside((int)Tp[0],(int)Tp[1],(int)Tp[2]))  val[i] = outval;
            else
            {
                typename BranchingImageTypes::VoxelIndex vi (in->index3Dto1D(Tp[0],Tp[1],Tp[2]), 0);
                bool found=false;
                    for(vi.offset = 0 ; vi.offset<img[vi.index1d].size() ; vi.offset++)
                    {
                        T v= in->imgList[this->time][vi.index1d][vi.offset][0]; // assume that labels are stored in first channel
                        for(unsigned int l = 0 ; l<lab.size() ; l++) if(v==lab[l]) found=true;
                        if(found) break;
                    }
                if(found)
                {
                    val[i]=vi.offset+1;
                    if(glob) {for(unsigned int i1d=0;i1d<vi.index1d;i1d++) val[i]+=img[i1d].size(); val[i]-=1;}
                }
                else val[i]=outval;
            }
        }

    }

    void handleEvent(sofa::core::objectmodel::Event *event)
    {
        if (simulation::AnimateEndEvent::checkEventType(event))
        {
            raBranchingImage in(this->branchingImage);
            raTransform inT(this->branchingImageTransform);

            // get current time modulo dimt
            const unsigned int dimt=in->getDimensions()[4];
            if(!dimt) return;
            Real t=inT->toImage(this->getContext()->getTime()) ;
            t-=(Real)((int)((int)t/dimt)*dimt);
            t=(t-floor(t)>0.5)?ceil(t):floor(t); // nearest
            if(t<0) t=0.0; else if(t>=(Real)dimt) t=(Real)dimt-1.0; // clamp

            if(this->time!=(unsigned int)t) { this->time=(unsigned int)t; update(); }
        }
    }

};


} // namespace engine
} // namespace component
} // namespace sofa

#endif // SOFA_BRANCHINGIMAGE_BranchingCellOffsetsFromPositions_H
