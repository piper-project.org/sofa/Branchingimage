#define SOFA_BRANCHINGIMAGE_BRANCHINGIMAGECONVERTER_CPP

#include "BranchingImageConverter.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS(ImageToBranchingImageConverter)

int ImageToBranchingImageConverterClass = core::RegisterObject("ImageToBranchingImageConverter")
        .add<ImageToBranchingImageConverter<unsigned char> >(true)
        .add<ImageToBranchingImageConverter<double> >()
        .add<ImageToBranchingImageConverter<unsigned char,double> >()
        .add<ImageToBranchingImageConverter<unsigned char,unsigned int> >()
#ifdef BUILD_ALL_IMAGE_TYPES
        .add<ImageToBranchingImageConverter<char> >()
        .add<ImageToBranchingImageConverter<int> >()
        .add<ImageToBranchingImageConverter<unsigned int> >()
        .add<ImageToBranchingImageConverter<short> >()
        .add<ImageToBranchingImageConverter<unsigned short> >()
        .add<ImageToBranchingImageConverter<long> >()
        .add<ImageToBranchingImageConverter<unsigned long> >()
        .add<ImageToBranchingImageConverter<float> >()
        .add<ImageToBranchingImageConverter<bool> >()
        .add<ImageToBranchingImageConverter<bool,unsigned char> >()
        .add<ImageToBranchingImageConverter<bool,double> >()
#endif
        ;

template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<unsigned char>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<double>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<unsigned char,double>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<unsigned char,unsigned int>;
#ifdef BUILD_ALL_IMAGE_TYPES
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<char>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<int>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<unsigned int>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<short>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<unsigned short>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<long>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<unsigned long>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<float>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<bool>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<bool,unsigned char>;
template class SOFA_BRANCHINGIMAGE_API ImageToBranchingImageConverter<bool,double>;
#endif



SOFA_DECL_CLASS(BranchingImageToImageConverter)

int BranchingImageToImageConverterClass = core::RegisterObject("BranchingImageToImageConverter")
        .add<BranchingImageToImageConverter<unsigned char> >(true)
        .add<BranchingImageToImageConverter<double> >()
        .add<BranchingImageToImageConverter<double,unsigned char> >()
        .add<BranchingImageToImageConverter<double,unsigned int> >()
        .add<BranchingImageToImageConverter<unsigned int, unsigned char> >()
#ifdef BUILD_ALL_IMAGE_TYPES
        .add<BranchingImageToImageConverter<char> >()
        .add<BranchingImageToImageConverter<int> >()
        .add<BranchingImageToImageConverter<unsigned int> >()
        .add<BranchingImageToImageConverter<short> >()
        .add<BranchingImageToImageConverter<unsigned short> >()
        .add<BranchingImageToImageConverter<long> >()
        .add<BranchingImageToImageConverter<unsigned long> >()
        .add<BranchingImageToImageConverter<float> >()
        .add<BranchingImageToImageConverter<bool> >()
#endif
        ;

template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<unsigned char>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<double>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<double,unsigned char>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<double,unsigned int>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<unsigned int, unsigned char>;
#ifdef BUILD_ALL_IMAGE_TYPES
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<char>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<int>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<unsigned int>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<short>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<unsigned short>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<long>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<unsigned long>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<float>;
template class SOFA_BRANCHINGIMAGE_API BranchingImageToImageConverter<bool>;
#endif


} //
} // namespace component

} // namespace sofa

