#define SOFA_BRANCHINGIMAGE_IMAGESAMPLER_CPP

#include "ImageSampler.h"
#include <image/ImageSampler.h>
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace defaulttype;

SOFA_DECL_CLASS (BranchingImageSampler);

int BranchingImageSamplerClass = core::RegisterObject("Samples an object represented by an image")
        .add<ImageSampler<BranchingImageB> >()
        .add<ImageSampler<BranchingImageUC> >()
        .add<ImageSampler<BranchingImageD> >()
        .add<ImageSampler<BranchingImageF> >()
        .add<ImageSampler<BranchingImageUS> >()
        ;

template class SOFA_BRANCHINGIMAGE_API ImageSampler<BranchingImageB>;
template class SOFA_BRANCHINGIMAGE_API ImageSampler<BranchingImageUC>;
template class SOFA_BRANCHINGIMAGE_API ImageSampler<BranchingImageD>;
template class SOFA_BRANCHINGIMAGE_API ImageSampler<BranchingImageF>;
template class SOFA_BRANCHINGIMAGE_API ImageSampler<BranchingImageUS>;



} //
} // namespace component

} // namespace sofa

